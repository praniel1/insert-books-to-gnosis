import PyPDF2
import re
from app import predict
import requests
import shutil
import glob
import sys
import os
import django
# from django.core.files.uploadedfile import SimpleUploadedFile M1]%70]lSe
from django.core.files import File
from urllib import request
from zipfile import ZipFile

sys.path.append('/projects/gnosis/django/gnosis/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'GnosisLibrary.settings'
django.setup()
from LivingLibrary.models import Book, Author, Genre, Language
from LivingLibrary.forms import AuthorForm, GenreForm, LanguageForm,BookForm

# print('This program is ok')
mainfolder = '/mnt/jan-16_feb-13/'
# mainfolder = 'Foundation Engineering Handbook 2nd Ed.pdf'
sys.setrecursionlimit(10000)

blacklist = ['zipfiles/freescale_download_package.zip','1.001 Best Places to Have Sex in America by Jennifer Hunt and Dan Baritchi.pdf','FM_3-24_Counterinsurgency.pdf',
'636 иC653.pdf','Complete Guide to Digital Infrared Photography - By Joe Farace.pdf']

def languagename(language):
	if language.lower() == 'en':
		return 'english'
	elif language.lower() == 'fr':
		return 'french'
	elif language.lower() == 'ja':
		return 'japanese'
	elif language.lower() == 'fa':
		return 'farsi'
	else:
		return language

def convert_isbn_number(x):
	isbnlist = []
	for i in x:
		for j in i:
			if len(j)>6:
				j = j.replace('-','')
				j = j.replace(' ','')
				isbnlist.append(j)
	if (len(isbnlist)>0):
		print("potential isbn numbers : ")
		print(isbnlist)
	return isbnlist

def cleandata(data):
	newdata = data.replace(' ','-')
	newdata = newdata.replace('/','')
	return newdata

def createfolders():
	if not os.path.exists('done'):
		os.makedirs('done')
	if not os.path.exists('notdone'):
		os.makedirs('notdone')
	if not os.path.exists('duplicate'):
		os.makedirs('duplicate')
	if not os.path.exists('filetypeunknown'):
		os.makedirs('filetypeunknown')

def deletezippedfile():
	if os.path.exists('done/zipfiles'):
		shutil.rmtree('done/zipfiles')
	if os.path.exists('notdone/zipfiles'):
		shutil.rmtree('notdone/zipfiles')
	if os.path.exists('duplicate/zipfiles'):
		shutil.rmtree('duplicate/zipfiles')

def pushothermodels(modelname ,modeldata):
	if modelname == 'language':
		langdict = {}
		langdict['languagename'] = modeldata
		form = LanguageForm(langdict)
		if form.is_valid():
			form.save()

	if modelname == 'genre':
		genredic = {}
		genredic['genrename'] = modeldata
		form = GenreForm(genredic)
		if form.is_valid():
			form.save()

	if modelname == 'authors':
		authorsdic = {}
		authors = modeldata
		for author in authors:
			authorsdic['authorname'] = author.strip()
			form = AuthorForm(authorsdic)
			if form.is_valid():
				form.save()

def pushtodatabase(bookdetails, name):
	pushothermodels('language', bookdetails['language'])
	pushothermodels('genre', bookdetails['genre'])
	pushothermodels('authors', bookdetails['authors'])

	bookdic = {}
	bookdic['title'] = bookdetails['title']
	bookdic['pageCount'] = bookdetails['pageCount']
	bookdic['isbn'] = bookdetails['isbn']
	bookdic['pages'] = bookdetails['pageCount']

	languageids = Language.objects.filter(languagename=bookdetails['language']).values('id')[0]['id']
	bookdic['language'] = str(languageids)
	
	genreid = Genre.objects.filter(genrename=bookdetails['genre']).values('id')[0]['id']
	bookdic['genre'] = [str(genreid)]
	
	authorlist = []
	for author in bookdetails['authors']:
		author = author.strip()
		authorid = Author.objects.filter(authorname=author.capitalize()).values('id')[0]['id']
		authorlist.append(str(authorid))
	bookdic['author'] = authorlist
	
	bookfiles = {}
	bookfiles['pdf'] = File(open(name,'rb'), bookdic['title']+os.path.splitext(name)[1])
	bookfiles['epub'] = File(open(name,'rb'), bookdic['title']+os.path.splitext(name)[1])

	testfile = request.URLopener()
	coverimagename = cleandata(bookdetails['title']+'.jpg')
	if len(coverimagename) > 20:
		coverimagename = cleandata(bookdetails['title'])
		coverimagename = coverimagename[:10]+'.jpg'
	testfile.retrieve(bookdetails['image'],coverimagename)
	bookfiles['cover'] = File(open(coverimagename,'rb'),coverimagename.replace('pdfbooks/',''))

	form = BookForm(bookdic,bookfiles)
	if form.is_valid():
		form.save()
	else:
		print(form.errors)
	try:
		os.remove(coverimagename)
	except Exception as er:
		print(er)

# gets the title authors page etc from google api and returns them
def getisbndetails(isbn):
	api = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'
	isbnapi = api+isbn
	r = requests.get(isbnapi)
	data = r.json()
	bookdetails = {}
	try :	
		bookdetails['title'] = data['items'][0]['volumeInfo']['title']
		bookdetails['authors'] = data['items'][0]['volumeInfo']['authors']
		bookdetails['pageCount'] = data['items'][0]['volumeInfo']['pageCount']
		bookdetails['genre'] = predict(data['items'][0]['volumeInfo']['title'])
		bookdetails['isbn'] = isbn
		bookdetails['image'] = data['items'][0]['volumeInfo']['imageLinks']['thumbnail']
		bookdetails['language'] = languagename(data['items'][0]['volumeInfo']['language'])
	except Exception as er :
		print(er)
		return None
	return bookdetails

def getbookdetails(name):
	#regex=r'978[-0-9]{10,15}'
	regex = r'(ISBN[-]*(1[03])*[ ]*(: ){0,1})*(([0-9Xx][- ]*){13}|([0-9Xx][- ]*){10})'
	found = 2 #1->done 2->not done 3->duplicate
	newdir= name.replace(mainfolder,'')
	try:
		pdfFileObj = open(name, 'rb')
	except Exception as error:
		print(error)
		try:
			shutil.move(name,'notdone/'+newdir)
		except Exception as e:
			print(e)
		return
	try:
		pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
	except Exception as er:
		print(er)
		try:
			shutil.move(name,'notdone/'+newdir)
		except Exception as er:
			print(er)
		return
	done = False
	for i in range(0,10): # read from pages 0 to 10 or 1 to 11
		try:
			page = pdfReader.getPage(i)
		except:
			continue
		try:
			x = re.findall(regex, page.extractText())
			x = convert_isbn_number(x)
		except:
			continue
		if x is not None: #if there are some isbn in current page
			for isbn in x:
				api = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'
				
				isbnapi = api+isbn
				r = requests.get(isbnapi)
				data = r.json()
				if 'items' in data:
					if 'volumeInfo' in data['items'][0]:
						ifisbnfound = Book.objects.filter(isbn=isbn)
						if ifisbnfound.count() == 1 :
							found = 3
						else:
							bookdetails = getisbndetails(isbn)
							if bookdetails != None:
								found = 1
								print("correct isbn found"+str(isbn))
						done = True
						break
		if done:
			break
	if found == 1:
		pushtodatabase(bookdetails, name)
		os.makedirs(os.path.dirname('done/'+newdir), exist_ok=True)
		shutil.move(name,'done/'+newdir)
		print('done')
	elif found == 2:
		os.makedirs(os.path.dirname('notdone/'+newdir), exist_ok=True)
		shutil.move(name,'notdone/'+newdir)
		print('isbn not found')
	else:
		os.makedirs(os.path.dirname('duplicate/'+newdir), exist_ok=True)
		shutil.move(name,'duplicate/'+newdir)
		print('its a duplicate')

def entrypoint(direc):
	folder = glob.glob(direc+"*")
	#folder = glob.glob('pdfbooks/*') gives array of files in folder
	for files in folder:
		#loop through all files
		file_name, file_ext = os.path.splitext(files)
		if (os.path.basename(files) in blacklist):
			shutil.move(files,'notdone/'+os.path.basename(files))
		elif file_ext == '':
			#if nothing then it is a directory
			dirname = os.path.basename(files)
			#dirname stores name of current directory
			newdir = direc+dirname+'/'
			# newdir = pdfbooks/aircraft/
			entrypoint(newdir)
			# after all files inside the diretory is moved then remove the directory
			try:
				shutil.rmtree(newdir)
			except Exception as er:
				print('Removing directory exception')
				print(er)
		elif file_ext == '.pdf':
			print(files)
			print(os.path.basename(files))
			getbookdetails(files)
		elif file_ext == '.zip':
			if files in blacklist:
				deletezippedfile()
				shutil.move(files,'done/'+os.path.basename(files))
			if not os.path.exists('zipfiles'):
				os.mkdir('zipfiles')
			try:
				print('upzipping :'+files)
				openedzippedfile = ZipFile(files)
				openedzippedfile.extractall('zipfiles')
				entrypoint('zipfiles/')
				#after all work is done in zipfiles then remove the folder in done, notdone and duplicate
				deletezippedfile()
				#remove the directry created earlier
				shutil.rmtree('zipfiles')
				# then move the file to done folder
				shutil.move(files,'done/'+os.path.basename(files))
			except Exception as er:
				print(er)
				try:
					shutil.move(files,'/mnt/insert-books-to-gnosis/filetypeunknown/'+os.path.basename(files))
				except Exception as er:
					print("zip exception")
					print(er)
		else:
			try:
				shutil.move(files,'/mnt/insert-books-to-gnosis/filetypeunknown/'+os.path.basename(files))
			except Exception as er:
				print("moving file type exception")
				print(er)



createfolders()
entrypoint(mainfolder)